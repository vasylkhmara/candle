<?php
require_once("./vendor/autoload.php");
use Viber\Client;
$apiKey = '4631b21a6075c4f7-05bb7d90c4be55f6-c0b8f3c5574ac021';
$webhookUrl = 'https://wise-ua.tk/bot-plus.php';
try {
    $client = new Client([ 'token' => $apiKey ]);
    $result = $client->setWebhook($webhookUrl);
    echo "Success!\n";
} catch (Exception $e) {
    echo "Error: ". $e->getMessage() ."\n";
}
