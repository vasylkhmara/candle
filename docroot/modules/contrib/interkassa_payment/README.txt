This module contains basic integration with Interkassa 2.0.
https://interkassa.com/

The module works with Payment module(https://drupal.org/project/payment).

INSTALLATION:
Before you'll start the installation process you must register on Interkassa
https://interkassa.com/ and create your own Checkout.

1) Download the module from Drupal.org and extract it to your modules folder.
2) Enable it.
3) Go to /admin/config/services/payment/method and enable Interkassa payment method configuration.
4) Edit payment method configuration
(Just click edit before 'Enable payment method: Interkassa payment').
5) Setup the settings according your data from Interkassa.

That's it :)

Additional information:
Default Success url - http://example.com/payment_offsite/interkassa/success
Default Fail url - http://example.com/payment_offsite/interkassa/fail
Default Pending url - http://example.com/payment_offsite/interkassa/pending
Default Interaction url - http://example.com/payment_offsite/interkassa/ipn
