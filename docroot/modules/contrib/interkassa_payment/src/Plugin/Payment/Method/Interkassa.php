<?php

namespace Drupal\interkassa_payment\Plugin\Payment\Method;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\payment\Entity\Payment;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimple;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimpleInterface;

/**
 * A Interkassa payment method.
 *
 * @PaymentMethod(
 *   id = "payment_interkassa",
 *   deriver = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteDeriver",
 *   operations_provider = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteOperationsProvider"
 * )
 */
class Interkassa extends PaymentMethodOffsiteSimple implements PaymentMethodOffsiteSimpleInterface {

  /**
   * {@inheritdoc}
   */
  public function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * Returns the ID of the payment method this plugin is for.
   *
   * @return string
   *   The configured entity ID.
   */
  public function getEntityId() {
    return $this->pluginDefinition['entity_id'];
  }

  /**
   * Returns configured currency.
   *
   * @return string
   *   The currency.
   */
  public function getCurrency() {
    $currency_code = $this->payment->getCurrencyCode();
    return isset($this->pluginDefinition['config']['ik_cur'][$currency_code]) ? $currency_code : $this->pluginDefinition['config']['ik_cur_default'];
  }

  /**
   * Return local payment status related to given remote.
   *
   * @param string $status
   *   Remote status id.
   *
   * @return string.
   *   Local status id.
   */
  public function getStatusId($status) {
    return $this->pluginDefinition['ipn_statuses'][$status];
  }

  /**
   * Returns action URL to submit data to.
   *
   * @return string
   *   The configured URL.
   */
  public function getAction() {
    return $this->pluginDefinition['config']['action_url'];
  }

  /**
   * Returns merchant ID in terms of Interkassa.
   *
   * @return string
   *   The merchant ID.
   */
  public function getCheckoutId() {
    return $this->pluginDefinition['config']['ik_co_id'];
  }

  /**
   * Returns configured has function.
   *
   * @return string
   *   The hash function.
   */
  public function getHashType() {
    return $this->pluginDefinition['config']['hash_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentStatusAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettablePaymentStatuses(AccountInterface $account, PaymentInterface $payment) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultPages() {
    return [
      'success' => FALSE,
      'fail' => TRUE,
      'pending' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function paymentForm() {
    $form = [];
    $payment = $this->getPayment();
    $form['#action'] = $this->getAction();

    $this->addPaymentFormData('ik_co_id', $this->getCheckoutId());
    $this->addPaymentFormData('ik_pm_no', $payment->id());
    $this->addPaymentFormData('ik_am', round($payment->getAmount(), 2));
    $this->addPaymentFormData('ik_cur', $this->getCurrency());
    $this->addPaymentFormData('ik_desc', $this->t('Order ID: @orderid, User mail: @mail', [
      '@orderid' => $payment->id(),
      '@mail' => $payment->getOwner()->getEmail(),
    ]));
    $this->addPaymentFormData('ik_sign', $this->getSignature());

    $form += $this->generateForm();

    return $form;
  }

  /**
   * Returns configured secret key.
   *
   * @param null|string $payway
   *   The payway used for payment.
   *
   * @return string
   *   Secret key.
   */
  public function getSecretKey($payway = NULL) {
    return (isset($payway) && $payway == 'test_interkassa_test_xts') ? $this->pluginDefinition['config']['test_key'] : $this->pluginDefinition['config']['sign_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSignature($signature_type = PaymentMethodOffsiteSimple::SIGN_OUT) {
    $data = $signature_type == PaymentMethodOffsiteSimple::SIGN_OUT ? $this->getPaymentFormData() : $this->request->request->all();
    unset($data['ik_sign']);
    if (!isset($data['ik_pw_via'])) {
      $data['ik_pw_via'] = NULL;
    }
    $secretkey = $this->getSecretKey($data['ik_pw_via']);
    ksort($data, SORT_STRING);
    array_push($data, $secretkey);
    $sign_string = implode(':', $data);
    $hash = hash($this->getHashType(), $sign_string, TRUE);
    return base64_encode($hash);
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantIdName() {
    return 'ik_co_id';
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionIdName() {
    return 'ik_pm_no';
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountName() {
    return 'ik_am';
  }

  /**
   * {@inheritdoc}
   */
  public function getSignatureName() {
    return 'ik_sign';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    if ($this->isFallbackMode()) {
      return [
        'ik_co_id',
        'ik_inv_id',
        'ik_inv_st',
        'ik_pm_no',
      ];
    }
    return [
      'ik_co_id',
      'ik_inv_id',
      'ik_inv_st',
      'ik_inv_crt',
      'ik_inv_prc',
      'ik_pm_no',
      'ik_pw_via',
      'ik_am',
      'ik_cur',
      'ik_ps_price',
      'ik_desc'
    ];
  }

  protected function getValidators() {
    if ($this->isFallbackMode()) {
      return [
        'validateEmpty',
        'validateRequiredKeys',
        'validateMerchant',
        'validateTransactionId',
      ];
    }
    return parent::getValidators();
  }

  /**
   * {@inheritdoc}
   */
  public function ipnExecute() {
    if (!$this->ipnValidate()) {
      // @todo replace with throw exceptions.
      return [
        'status' => 'fail',
        'message' => '',
        'response_code' => 200,
      ];
    }

    $ik_inv_st = $this->request->request->get('ik_inv_st');
    $payment_status = $this->getStatusId($ik_inv_st);
    $status = isset($payment_status) ? $payment_status : 'payment_pending';
    $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($status));
    $this->getPayment()->save();

    return [
      'status' => 'success',
      'message' => '',
      'response_code' => 200,
    ];
  }

  /**
   * {@inheritdoc}
   */
  function isConfigured() {
    return !empty($this->getMerchantId()) && !empty($this->getAction());
  }

  /**
   * Returns success message.
   *
   * @return array
   *   The renderable array.
   */
  public function getSuccessContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', ['@external_status' => 'success']),
    ];
  }

  /**
   * Returns fail message.
   *
   * @return array
   *   The renderable array.
   */
  public function getFailContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', ['@external_status' => 'FAIL']),
    ];
  }

  /**
   * Returns pending message.
   *
   * @return array
   *   The renderable array.
   */
  public function getPandingContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', ['@external_status' => 'PENDING']),
    ];
  }

}
