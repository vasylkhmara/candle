(function ($) {

    Drupal.behaviors.myMainSliderBehavior = {
        attach: function (context, settings) {
            $('.sliderow').bxSlider({
                auto: true
            });
        }
    };

    Drupal.behaviors.myNewsSliderBehavior = {
        attach: function (context, settings) {
            $('.news .view-content').bxSlider({
                auto: true
            });
        }
    };

    Drupal.behaviors.identifyUserBehavior = {
        attach: function (context, settings) {
            $.get("http://ipinfo.io", function(response) {
                if (response.country === 'UA') {
                    $(".vkwrap").remove();
                }
                 console.log(response);
            }, "json");

        }
    };

})(jQuery);
